module type CUSTOMQUEUE = sig

(** 
  * A queue contains elements of type 'a - FIFO 
  * Follows a FIFO style convention
  *)
type 'a t

exception Empty

(* Create and return a new empty queue *)
val create : unit -> 'a t

(* Add e q adds element e to the end of queue q *)
val push : 'a -> 'a t -> unit

(** Removes and returns the first element in the queue
  * Raises Empty if the queue is empty 
  *)
val pop : 'a t -> 'a

(** Returns the first element in the queue without removing it
  * Raises Empty if the queue is empty
  *)
val peek : 'a t -> 'a

(* Removes all elemenets from the queue *)
val clear : 'a t -> unit

(* Returns true if the queue is empty, false otherwise *)
val is_empty : 'a t -> bool

(* Returns the number of elements in the queue *)
val length : 'a t -> int

end