module CustomQueue : CUSTOMQUEUE = struct

exception Empty = Queue.Empty

type 'a t = 'a Queue.t

let create = Queue.create

let is_empty = Queue.is_empty

let push x q = Queue.push x q

let pop q = if is_empty q then raise Empty else Queue.pop q

let peek q = if is_empty q then raise Empty else Queue.peek q

let clear q = Queue.clear q

let length q = Queue.length q

end





