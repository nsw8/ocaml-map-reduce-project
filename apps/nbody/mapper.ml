let (id1, x) = Program.get_input() in
let b : Util.body = Util.unmarshal x in
let (mass1, position1, _) = b in
let bodies : (string * Util.body) list = Util.unmarshal (Program.get_shared_data()) in
let helper acc other =
  let (id2, (mass2, position2, _)) = other in
  let dist = Plane.distance position1 position2 in
  let g_force = (Util.cBIG_G *. mass1) /. (dist *. dist) in
  let direction = Plane.unit_vector position2 position1 in
  let result = Util.marshal (Plane.scale_point g_force direction) in
  if id1 = id2 then acc
  else (other, result)::acc in
Program.set_output (List.fold_left helper []
                   bodies)
