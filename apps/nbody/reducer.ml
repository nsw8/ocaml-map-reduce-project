let (key, values) = Program.get_input() in
let helper acc v =
  Plane.v_plus acc (Util.unmarshal v) in
let acceleration = List.fold_left helper (0., 0.) values in
let result =
  let (id, (mass, location, velocity)) = key in
  let new_position =
    Plane.v_plus (Plane.v_plus location velocity) (Plane.scale_point 0.5 acceleration) in
  let new_velocity = Plane.v_plus velocity acceleration in
  (id, (mass, new_position, new_velocity)) in
Program.set_output [Util.marshal result]