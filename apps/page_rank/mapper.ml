let (key, value) = Program.get_input() in
let ht = Util.unmarshal (Program.get_shared_data()) in
let website : Util.website = Util.unmarshal key in
let num_links = float_of_int (List.length website.Util.links) in
let rank = float_of_string value in
let weight = if num_links = 0. then 0. else rank /. num_links in
Program.set_output (List.fold_left (fun acc k -> (Util.marshal (Hashtbl.find ht k), string_of_float weight)::acc) [] website.Util.links)
