let (key, values) = Program.get_input() in
let helper acc str =
  let v = int_of_string str in
  match acc with
  | [] -> [v]
  | _::_ -> if List.mem v acc then acc else v::acc in
let indices = List.sort (fun a b -> a - b) (List.fold_left helper [] values) in
let strings = List.map string_of_int indices in
    Program.set_output strings
